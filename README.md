
# Rust AVR blink

Arduino (Uno) blink example (copied from [here](https://dev.to/creativcoder/how-to-run-rust-on-arduino-uno-40c0))
written in Rust.

# Requires

* rustc + cargo.
* The ``nightly-2021-01-07`` toolchain (as of 2021/01/29, latest nightly fails to compile).
* ``avr-gcc`` and ``avrdude``.

# Compiling and uploading with cargo

```bash
$ cargo run --package atmega328p-blink-hal --bin atmega328p-blink-hal --release
```

## Bibliography

* [Rust Embedded Book](https://docs.rust-embedded.org/book/intro/index.html)
* [How to run Rust on Arduino Uno](https://dev.to/creativcoder/how-to-run-rust-on-arduino-uno-40c0)
* [AVR HAL + Cargo.toml optimization hints](https://github.com/Rahix/avr-hal)
* [AVR specifications](https://github.com/Rahix/avr-hal/tree/master/avr-specs)
* [Rust install custom toolchains](https://doc.rust-lang.org/nightly/edition-guide/rust-2018/rustup-for-managing-rust-versions.html)
* ['LLVM ERROR: Not supported instr' error cause](https://github.com/rust-lang/compiler-builtins/issues/400#issuecomment-763898670)
* [Example Blog](https://blog.cecton.com/posts/rust-and-arduino-part1/)